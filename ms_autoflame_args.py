import pyautogui
import time
from random import randint
import argparse, sys

import numpy as np
import os
from mss import mss
import cv2
from PIL import Image
import pytesseract
import re
from termcolor import colored

parser=argparse.ArgumentParser()

parser.add_argument('--main', help='Target Main Stat(str, dex, int, luk, dagger, xenon)')
parser.add_argument('--upgraded', help='Use if upgraded', action='store_true')
parser.add_argument('--no-upgraded', help='Use if not upgraded', action='store_true')
parser.add_argument('--weapon', help='Use if weapon', action='store_true')
parser.add_argument('--no-weapon', help='Use if not weapon', action='store_true')
parser.add_argument('--calibrate', help='Debug mode?', action='store_true')
parser.add_argument('--target', help='Target Flame Score', type=float)
parser.add_argument('--att_score', help='Value of ATT, default 1att = 2stat', type=float)
parser.add_argument('--all_score', help='Value of 1%% all stats, default 1%% = 9stat', type=float)
parser.add_argument('--bd_score', help='Value of 1%% Boss Dmg, default 1%% = 8stat', type=float)
parser.add_argument('--dmg_score', help='Value of 1%% Dmg, default 1%% = 16stat', type=float)
parser.add_argument('--xenon_mul', help='Xenon main stat score multiplier, default 0.6', type=float)
args=parser.parse_args()

TARGET_MAIN_STAT = args.main or input("Target Main Stat(str, dex, int, luk, dagger, xenon):")
IS_UPGRADED = False if args.no_upgraded else args.upgraded or (input("Is the equipment upgraded? t/f:") == "t")
IS_WEAPON = False if args.no_weapon else args.weapon or (input("Is this a weapon? t/f:") == "t")
MODE_CALIBRATE = args.calibrate or False
TARGET_FLAME_SCORE = args.target or float(input("Target Flame Score:"))
ATT_SCORE = args.att_score or 2.0
ALL_SCORE = args.all_score or 9.0
BD_SCORE = args.bd_score or 8.0
DMG_SCORE = args.dmg_score or 16.0
XENON_MULTIPLIER = args.xenon_mul or 0.6
CALIBRATION_STATS = {
    'str': ["str", "dex", "ack PowEr"],
    'dex': ["str", "dex", "ack PowEr"],
    'int': ["luk", "magic a"],
    'luk': ["dex", "lUK", "ack PowEr"],
    'dagger': ["dex", "lUK", "ack PowEr"],
    'xenon': ["dex", "ack PowEr"]
}[TARGET_MAIN_STAT]

def getTotalFlameScore(statsDict):
    if TARGET_MAIN_STAT == 'str':
        MAIN = getFlameStat(statsDict, "STR", IS_UPGRADED)
        SEC = getFlameStat(statsDict, "DEX", IS_UPGRADED)
        ATT = getFlameStat(statsDict, "ack Power", IS_UPGRADED) * ATT_SCORE
    elif TARGET_MAIN_STAT == 'dex':
        SEC = getFlameStat(statsDict, "STR", IS_UPGRADED)
        MAIN = getFlameStat(statsDict, "DEX", IS_UPGRADED)
        ATT = getFlameStat(statsDict, "ack Power", IS_UPGRADED) * ATT_SCORE
    elif TARGET_MAIN_STAT == 'int':
        MAIN = max(getFlameStat(statsDict, "NT", IS_UPGRADED), 
                getFlameStat(statsDict, "WT", IS_UPGRADED))
        SEC = getFlameStat(statsDict, "LUK", IS_UPGRADED)
        ATT = getFlameStat(statsDict, "Magic A", IS_UPGRADED) * ATT_SCORE
    elif TARGET_MAIN_STAT in ['luk', 'dagger']:
        SEC = getFlameStat(statsDict, "DEX", IS_UPGRADED)
        if TARGET_MAIN_STAT == 'dagger':
            SEC += getFlameStat(statsDict, "STR", IS_UPGRADED)
        MAIN = getFlameStat(statsDict, "LUK", IS_UPGRADED)
        ATT = getFlameStat(statsDict, "ack Power", IS_UPGRADED) * ATT_SCORE
    elif TARGET_MAIN_STAT == 'xenon':
        MAIN = getFlameStat(statsDict, "STR", IS_UPGRADED)
        MAIN += getFlameStat(statsDict, "DEX", IS_UPGRADED)
        MAIN += getFlameStat(statsDict, "LUK", IS_UPGRADED)
        MAIN *= XENON_MULTIPLIER
        SEC = 0
        ATT = getFlameStat(statsDict, "ack Power", IS_UPGRADED) * ATT_SCORE
    else:
        print("Target Main Stat Error" + str(TARGET_MAIN_STAT))
        exit()
    ALL = getFlameStat(statsDict, "Stats", False) * ALL_SCORE

    flameScore = MAIN + (0.1 * SEC) + ATT + ALL

    if IS_WEAPON:
        BOSS = getFlameStat(statsDict, "Boss Damage", False) * BD_SCORE
        DAMAGE = getFlameStat(statsDict, "Damage", False, exactMatch = True) * DMG_SCORE
        flameScore += BOSS + DAMAGE

    return flameScore

##### Calibration/Performance #####
FAILSAFE = True # End program by moving mouse to screen corner
STUCK_LIMIT = 40
DELAY_BEFORE_READING_FLAME = 0.2 # Delay to wait for server to update stats
KEEP_STATS_VISIBLE_DELAY = 0 # Extend the time the stats is manually visible

# Stats display on mouse hover
FLAME_REGION_X = 38
FLAME_REGION_Y = 255
FLAME_WIDTH = 205
FLAME_HEIGHT = 340

# EQUIP inventory tab
EQUIP_X = 26
EQUIP_Y = 61

# USE inventory tab
USE_X = 56
USE_Y = 61

# Slot to use flame on
SLOT_X = 34
SLOT_Y = 97

# Randomize mouse position up to x pixels to automatically refresh OCR visibility
WIGGLE = 6

############### END Settings ###############

LINE_REGEX = r"(?P<stat>[^:+\n]*)[ :+#]+(?P<total>\d+)[% ]*([([{]?(?P<base>\d+)\%?[ +]*(?P<bonus1>\d*)\%?[ +]*(?P<bonus2>\d*)\%?[)\]}]?)?"

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def main():
    pyautogui.FAILSAFE = FAILSAFE

    i = 0
    stuckCounter = STUCK_LIMIT
    flame = None
    lastScore = -1
    while True:
        print(colored("Checking flame: " + str(i),'cyan'))

        if MODE_CALIBRATE:
            while True:
                pyautogui.moveTo(SLOT_X+randint(-WIGGLE, WIGGLE),SLOT_Y+randint(-WIGGLE, WIGGLE))
                time.sleep(1)
                img, lines = getFlameText()
                statsDict = getFlameStats(lines)
                cv2.imshow('test', img)
                if cv2.waitKey(1) != -1:
                    break
                clearConsole()
                
                print("Raw Text:")
                print(lines)

                if isAllCalibrationStatsVisible(statsDict):
                    if isLineVisible("stats", lines) and not isStatVisible("stat", statsDict):
                        print(colored("Calibration Failed", 'red'))
                    else:
                        print(colored("Calibration Passed", 'cyan'))
                else:
                    print(colored("Calibration Failed", 'red'))

                for stat in CALIBRATION_STATS:
                    if isStatVisible(stat, statsDict):
                        print(colored(stat, 'green'))
                    else:
                        print(colored(stat, 'yellow'))
                
                if isLineVisible("stats", lines):
                    if not isStatVisible("stat", statsDict):
                        print(colored("All Stats Without Bonus", 'red'))
                    else:
                        print(colored("All Stats", 'green'))

                print("Visible Stats:")
                print("Visible Flame Score: " + str(getTotalFlameScore(statsDict)) + " / " + str(TARGET_FLAME_SCORE))
            break
        
        try:
            # Get Stats
            # Hover mouse to trigger stats screen
            pyautogui.moveTo(SLOT_X+randint(-WIGGLE, WIGGLE),SLOT_Y+randint(-WIGGLE, WIGGLE))

            img, lines = getFlameText()
            statsDict = getFlameStats(lines)

            if not isAllCalibrationStatsVisible(statsDict):
                stuckCounter -= 1
                if stuckCounter <= 0:
                    print("Force exit, CALIBRATION_STATS failed")
                    break
                if stuckCounter % 5 == 3:
                    pyautogui.press('Enter')
                print(colored("Some stats is not visible; stuck counter: " + str(stuckCounter), 'red'))
                print(lines)

                time.sleep(0.2)
                pyautogui.click(EQUIP_X,EQUIP_Y)
                continue

            if not isStatsValid(lines, statsDict):
                stuckCounter -= 1
                if stuckCounter <= 0:
                    print("Force exit, % stats failed")
                    break
                print(colored("Invalid % stats. Rechecking...", 'red'))
                print(lines)
                time.sleep(0.2)
                continue
            stuckCounter = STUCK_LIMIT

            # Check stats
            flameScore = getTotalFlameScore(statsDict)
        except Exception as e:
            print(e)
            stuckCounter -= 1
            if stuckCounter <= 0:
                break
            continue

        print("Flame Score: " + str(flameScore) + " / " + str(TARGET_FLAME_SCORE))
        if flameScore == lastScore:
            # Stats not updated or exactly same score, recheck
            print(colored("Same score as last flame? Rechecking..", 'red'))
            lastScore = -1
            continue
        lastScore = flameScore
        if flameScore >= TARGET_FLAME_SCORE:
            break

        time.sleep(KEEP_STATS_VISIBLE_DELAY)

        pyautogui.click(USE_X,USE_Y)
        if not flame:
            flame = pyautogui.locateCenterOnScreen('Flame.png', grayscale=True, region=(0,0,700,500))
            print(flame)
            if not flame:
                stuckCounter -= 1
                if stuckCounter <= 0:
                    print("Force exit, No flames detected")
                    return
                continue
        pyautogui.click(flame.x, flame.y, clicks=2)

        # Apply to item
        while not pyautogui.locateCenterOnScreen('TAB_EQUIP_SELECTED.png', grayscale=True, region=(0,0,100,100)):
            stuckCounter -= 1
            if stuckCounter <= 0:
                print("Force exit, could not use flame")
                return
                
            print("Unstucking use flame")
            if stuckCounter % 5 == 2:
                pyautogui.press('Enter')
                pyautogui.typewrite('@check '+ str(randint(1,9)))
                pyautogui.press('Enter', presses=2)
            time.sleep(0.5)
            pyautogui.click(USE_X,USE_Y)
            flame = pyautogui.locateCenterOnScreen('Flame.png', grayscale=True, region=(0,0,700,500))
            if not flame:
                stuckCounter -= 1
                print("No flames detected")
                continue
            pyautogui.click(flame.x, flame.y, clicks=2)
        pyautogui.click(SLOT_X, SLOT_Y)
        pyautogui.press('Enter', presses=2)

        stuckCounter = STUCK_LIMIT
        i += 1
        time.sleep(DELAY_BEFORE_READING_FLAME)

    print("Done: ~" + str(i) + " Flames used")

def isStatsValid(lines, statsDict):
    if isLineVisible("stats", lines) and (not isStatVisible("stat", statsDict) or not isStatBetween(statsDict, "Stats", 1, 7)):
        return False
    if IS_WEAPON:
        if not isStatBetween(statsDict, "Damage", 0, 7, exactMatch = True):
            return False
        if not isStatBetween(statsDict, "Boss Damage", 0, 14):
            return False

    return True

def isStatBetween(statsDict, stat, lower, higher, exactMatch = False):
    value = getFlameStat(statsDict, stat, False, exactMatch = exactMatch)
    return value >= lower and value <= higher

def isAllCalibrationStatsVisible(statsDict):
    return all(isStatVisible(stat, statsDict) for stat in CALIBRATION_STATS)

def isLineVisible(substring, lines):
    return any(substring.lower() in line.lower() for line in lines)

def getFlameStat(statsDict, stat, scrolled, exactMatch = False):
    if not exactMatch:
        # Get first matching key containing substring stat
        matched_lines = isStatVisible(stat, statsDict)
        if not matched_lines:
            return 0
        stats = matched_lines[0]
    else:
        dictValues = statsDict.get(stat)
        if not dictValues:
            return 0
        stats = dictValues

    # LUK: 0 (1 + 2 + 3)
    # stats[0] = total
    # stats[1] = base
    # stats[2] = flameBonus/scrollBonus if scrolled
    # stats[3] = scrollBonus

    if stat == "Stats":
        flameBonus = min(int(stats[0]), int(stats[2]))
        print(stat + " +" + str(flameBonus))
        return flameBonus
    if not stats[2]: # line has no bonus
        return 0
    if scrolled and not stats[3]: # Scrolled without flame bonus
        return 0

    flameBonus = int(stats[2])

    print(stat + " +" + str(flameBonus))
    return flameBonus

def isStatVisible(stat, statsDict):
    return [value for key, value in statsDict.items() if stat.lower() in key.lower()]

def getFlameStats(lines):
    statsDict = {}
    for line in lines:
        matches = re.search(LINE_REGEX, line)
        if matches:
            statsDict[matches["stat"].strip()] = [matches["total"], matches["base"], matches["bonus1"], matches["bonus2"]]
    if MODE_CALIBRATE:
        print(statsDict)
    return statsDict

def getFlameText():
    img = np.array(pyautogui.screenshot(region=(FLAME_REGION_X,FLAME_REGION_Y, FLAME_WIDTH, FLAME_HEIGHT)))

    img = cv2.bitwise_not(img)
    size = 3
    img = cv2.resize(img, (FLAME_WIDTH * size, FLAME_HEIGHT * size), cv2.INTER_CUBIC)
    
    ret, img = cv2.threshold(img, 160,255, cv2.THRESH_BINARY)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, img = cv2.threshold(img, 160,255, cv2.THRESH_BINARY)

    text = pytesseract.image_to_string(img, config="--psm 6")
    text = os.linesep.join([s for s in text.splitlines() if s and s != " " and s != "  "])

    # Exclude lines after Potential
    lines = text.split('\n')
    poten_index = index_containing_substring(lines, "Poten")
    if (poten_index >= 0):
        lines = lines[:poten_index]

    # # Exclude lines after Remaining Enhancements
    remaining_index = index_containing_substring(lines, "Enhancements")
    if (remaining_index >= 0):
        lines = lines[:remaining_index]

    # Exclude lines before Type
    type_index = index_containing_substring(lines, "Type")
    if (type_index >= 0):
        lines = lines[type_index:]

    return img, lines

def clearConsole():
    os.system('cls' if os.name == 'nt' else 'clear')

# https://stackoverflow.com/questions/2170900/get-first-list-index-containing-sub-string
def index_containing_substring(the_list, substring):
    for i, s in enumerate(the_list):
        if substring in s:
              return i
    return -1

if __name__ == "__main__":
    main()