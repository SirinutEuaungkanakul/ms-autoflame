import pyautogui
import time
from random import randint

import numpy as np
import os
from mss import mss
import cv2
from PIL import Image
import pytesseract
import re
from termcolor import colored

# Setup default settings
# 0. See Tutorial1.png
# 1. Use 1366x768 windowed
# 2. Move game screen to top left of your screen
# 3. Move inventory screen to snap lock top left of screen
# 4. Put item in top left slot of EQUIP
# 5. Have red flame somewhere visible in USE
# 6. (Optional) Change Maplestory font to something more readable for better accuracy
# ---- https://forum.ragezone.com/f428/changing-maplestorys-font-695367/
# ---- Personally I use Tahoma Bold but it still isn't 100% accurate

# Usages
# 1. Adjust target and/or flamescore calculation
# ---1--- TARGET_FLAME_SCORE
# ---2--- def getTotalFlameScore(lines): - Customize flamescore calculation + specify if item is scrolled
# ---------- See Tutorial2.png
# ---3--- CALIBRATION_LINES
# 2. Make sure game screen is fully visible then run code with MODE_CALIBRATE = True, 
# --- make sure the console display detected stats correctly
# 3. Change MODE_CALIBRATE = False then run the program again to start flaming

##### Settings #####
## For all possible combinations / Terminology see https://docs.google.com/document/d/1shvjaRT8I9McNmMHWHjILNdEidAZaw5wkkOrwm5gWW4/edit#
# Reference Target Flame Score (Equivalent flames are automatically captured)
# Item Max main stats 3L/4L
# lv120+HT 77/105
# lv140+Gollux/CRA 84/112
# lv160+Abso/Book/Berserk/EPatch 98/133
# lv200+Arcane/Dreamy 119/161
# Example score For arcane weapons:
# - Target T7/92 ATT => 92 * 3 = 276
# - Target T6 BOSS => 12% * 8 = 96
# - Target T6 DMG => 6% * 16 = 96
# - Target T5 AS => 5% * 9 = 45
# TARGET_FLAME_SCORE = 340 + 112 + 96 # Weapon T7 + T6 Boss + T6 DMG
# TARGET_FLAME_SCORE = 114 + 63
TARGET_FLAME_SCORE = 84 + 70 # 134/7
AMOUNT_TO_FLAME = 20000
MODE_CALIBRATE = False # True = View Only, False = AutoFlame
# These line(s) must be clearly visible, change to what is always visible in your item + relevant to your job
CALIBRATION_STATS = ["str", "dex", "ack PowEr"] # STR/DEX
# CALIBRATION_STATS = ["luk", "magic a"] # INT
# CALIBRATION_STATS = ["dex", "lUK", "ack PowEr"] # LUK
# CALIBRATION_STATS = ["str", "dex", "lUK", "ack PowEr", "magic a"] # All
##### Target Stats #####
def getTotalFlameScore(statsDict):
    ### getFlameStat(statsDict, <String to match>, <line is scrolled>) * <score per unit>
    STR = getFlameStat(statsDict, "STR", True) * 1
    DEX = getFlameStat(statsDict, "DEX", True) * 1
    INT = max(getFlameStat(statsDict, "NT", False), getFlameStat(statsDict, "WT", False)) * 1
    LUK = getFlameStat(statsDict, "LUK", False) * 1
    ATT = getFlameStat(statsDict, "ack Power", True) * 2
    MATT = getFlameStat(statsDict, "Magic A", False) * 2
    ALL = getFlameStat(statsDict, "Stats", False) * 10

    # 6x to replace T7 Flat, 8x to prioritize
    BOSS = getFlameStat(statsDict, "Boss Damage", False) * 8
    # 12x to replace T7 Flat, 16x to prioritize
    DAMAGE = getFlameStat(statsDict, "Damage", False, exactMatch = True) * 16

    # Adjust your flame score calculation here (or Select one)

    # flameScore = STR + (0.1 * DEX) + ATT + ALL # STR Armor
    flameScore = DEX + (0.1 * STR) + ATT + ALL # DEX Armor
    # flameScore = INT + (0.1 * LUK) + MATT + ALL # INT Armor
    # flameScore = LUK + (0.1 * DEX) + ATT + ALL # LUK Armor
    # flameScore = LUK + (0.1 * DEX) + (0.1 * STR) + ATT + ALL # Dagger Class Armor

    # flameScore = STR + (0.1 * DEX) + ATT + ALL + BOSS + DAMAGE # STR Weapon
    # flameScore = DEX + (0.1 * STR) + ATT + ALL + BOSS + DAMAGE # DEX Weapon
    # flameScore = INT + (0.1 * LUK) + MATT + ALL + BOSS + DAMAGE # INT Weapon
    # flameScore = LUK + (0.1 * DEX) + ATT + ALL + BOSS + DAMAGE # LUK Weapon
    # flameScore = LUK + (0.1 * DEX) + (0.1 * STR) + ATT + ALL + BOSS + DAMAGE # Dagger

    # flameScore = max( # Flame for any job
    #       STR + (0.1 * DEX) + ATT + ALL - 10,
    #       DEX + (0.1 * STR) + ATT + ALL,
    #       INT + (0.1 * LUK) + MATT + ALL - 10,
    #       LUK + (0.1 * DEX) + (0.1 * STR) + ATT + ALL - 10,
    #       (STR + DEX + LUK) / 1.6 + ALL # XENON
    #       )

    return flameScore
##### Calibration/Performance #####
FAILSAFE = True # End program by moving mouse to screen corner
STUCK_LIMIT = 40
DELAY_BEFORE_READING_FLAME = 0.2 # Delay to wait for server to update stats
KEEP_STATS_VISIBLE_DELAY = 0 # Extend the time the stats is manually visible

# Stats display on mouse hover
FLAME_REGION_X = 38
FLAME_REGION_Y = 255
FLAME_WIDTH = 205
FLAME_HEIGHT = 340

# EQUIP inventory tab
EQUIP_X = 26
EQUIP_Y = 61

# USE inventory tab
USE_X = 56
USE_Y = 61

# Slot to use flame on
SLOT_X = 34
SLOT_Y = 97

# Randomize mouse position up to x pixels to automatically refresh OCR visibility
WIGGLE = 6

############### END Settings ###############

LINE_REGEX = r"(?P<stat>[^:+\n]*)[ :+#]+(?P<total>\d+)[% ]*([([{]?(?P<base>\d+)\%?[ +]*(?P<bonus1>\d*)\%?[ +]*(?P<bonus2>\d*)\%?[)\]}]?)?"

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def main():
    pyautogui.FAILSAFE = FAILSAFE

    countdownStart()

    i = 0
    stuckCounter = STUCK_LIMIT
    flame = None
    lastScore = -1
    while True:
        print(colored("Checking flame: " + str(i),'cyan'))

        if MODE_CALIBRATE:
            while True:
                pyautogui.moveTo(SLOT_X+randint(-WIGGLE, WIGGLE),SLOT_Y+randint(-WIGGLE, WIGGLE))
                time.sleep(1)
                img, lines = getFlameText()
                statsDict = getFlameStats(lines)
                cv2.imshow('test', img)
                if cv2.waitKey(1) != -1:
                    break
                clearConsole()
                
                print("Raw Text:")
                print(lines)

                if isAllCalibrationStatsVisible(statsDict):
                    if isLineVisible("stats", lines) and not isStatVisible("stat", statsDict):
                        print(colored("Calibration Failed", 'red'))
                    else:
                        print(colored("Calibration Passed", 'cyan'))
                else:
                    print(colored("Calibration Failed", 'red'))

                for stat in CALIBRATION_STATS:
                    if isStatVisible(stat, statsDict):
                        print(colored(stat, 'green'))
                    else:
                        print(colored(stat, 'yellow'))
                
                if isLineVisible("stats", lines):
                    if not isStatVisible("stat", statsDict):
                        print(colored("All Stats Without Bonus", 'red'))
                    else:
                        print(colored("All Stats", 'green'))

                print("Visible Stats:")
                print("Visible Flame Score: " + str(getTotalFlameScore(statsDict)) + " / " + str(TARGET_FLAME_SCORE))
            break
        
        try:
            # Get Stats
            # Hover mouse to trigger stats screen
            pyautogui.moveTo(SLOT_X+randint(-WIGGLE, WIGGLE),SLOT_Y+randint(-WIGGLE, WIGGLE))

            img, lines = getFlameText()
            statsDict = getFlameStats(lines)

            if not isAllCalibrationStatsVisible(statsDict):
                stuckCounter -= 1
                if stuckCounter <= 0:
                    print("Force exit, CALIBRATION_STATS failed")
                    break
                if stuckCounter % 5 == 3:
                    pyautogui.press('Enter')
                print(colored("Some stats is not visible; stuck counter: " + str(stuckCounter), 'red'))
                print(lines)

                time.sleep(0.2)
                pyautogui.click(EQUIP_X,EQUIP_Y)
                continue

            if not isStatsValid(lines, statsDict):
                stuckCounter -= 1
                if stuckCounter <= 0:
                    print("Force exit, % stats failed")
                    break
                print(colored("Invalid % stats. Rechecking...", 'red'))
                print(lines)
                time.sleep(0.2)
                continue
            stuckCounter = STUCK_LIMIT

            # Check stats
            flameScore = getTotalFlameScore(statsDict)
        except:
            stuckCounter -= 1
            if stuckCounter <= 0:
                break
            continue

        print("Flame Score: " + str(flameScore) + " / " + str(TARGET_FLAME_SCORE))
        if flameScore == lastScore:
            # Stats not updated or exactly same score, recheck
            print(colored("Same score as last flame? Rechecking..", 'red'))
            lastScore = -1
            continue
        lastScore = flameScore
        if flameScore >= TARGET_FLAME_SCORE:
            break

        time.sleep(KEEP_STATS_VISIBLE_DELAY)

        # Reflame
        if i >= AMOUNT_TO_FLAME:
            print("out of flame budget :(")
            break

        pyautogui.click(USE_X,USE_Y)
        if not flame:
            flame = pyautogui.locateCenterOnScreen('Flame.png', grayscale=True, region=(0,0,700,500))
            print(flame)
            if not flame:
                stuckCounter -= 1
                if stuckCounter <= 0:
                    print("Force exit, No flames detected")
                    return
                continue
        pyautogui.click(flame.x, flame.y, clicks=2)

        # Apply to item
        while not pyautogui.locateCenterOnScreen('TAB_EQUIP_SELECTED.png', grayscale=True, region=(0,0,100,100)):
            stuckCounter -= 1
            if stuckCounter <= 0:
                print("Force exit, could not use flame")
                return
                
            print("Unstucking use flame")
            if stuckCounter % 5 == 2:
                pyautogui.press('Enter')
                pyautogui.typewrite('@check '+ str(randint(1,9)))
                pyautogui.press('Enter', presses=2)
            time.sleep(0.5)
            pyautogui.click(USE_X,USE_Y)
            flame = pyautogui.locateCenterOnScreen('Flame.png', grayscale=True, region=(0,0,700,500))
            if not flame:
                stuckCounter -= 1
                print("No flames detected")
                continue
            pyautogui.click(flame.x, flame.y, clicks=2)
        pyautogui.click(SLOT_X, SLOT_Y)
        pyautogui.press('Enter', presses=2)

        stuckCounter = STUCK_LIMIT
        i += 1
        time.sleep(DELAY_BEFORE_READING_FLAME)

    print("Done: ~" + str(i) + " Flames used")

def isStatsValid(lines, statsDict):
    if isLineVisible("stats", lines) and (not isStatVisible("stat", statsDict) or not isStatBetween(statsDict, "Stats", 1, 7)):
        return False
    if not isStatBetween(statsDict, "Damage", 0, 7, exactMatch = True):
        return False
    if not isStatBetween(statsDict, "Boss Damage", 0, 14):
        return False

    return True

def isStatBetween(statsDict, stat, lower, higher, exactMatch = False):
    value = getFlameStat(statsDict, stat, False, exactMatch = exactMatch)
    return value >= lower and value <= higher

def isAllCalibrationStatsVisible(statsDict):
    return all(isStatVisible(stat, statsDict) for stat in CALIBRATION_STATS)

def isLineVisible(substring, lines):
    return any(substring.lower() in line.lower() for line in lines)

def getFlameStat(statsDict, stat, scrolled, exactMatch = False):
    if not exactMatch:
        # Get first matching key containing substring stat
        matched_lines = isStatVisible(stat, statsDict)
        if not matched_lines:
            return 0
        stats = matched_lines[0]
    else:
        dictValues = statsDict.get(stat)
        if not dictValues:
            return 0
        stats = dictValues

    # LUK: 0 (1 + 2 + 3)
    # stats[0] = total
    # stats[1] = base
    # stats[2] = flameBonus/scrollBonus if scrolled
    # stats[3] = scrollBonus

    if stat == "Stats":
        flameBonus = min(int(stats[0]), int(stats[2]))
        print(stat + " +" + str(flameBonus))
        return flameBonus
    if not stats[2]: # line has no bonus
        return 0
    if scrolled and not stats[3]: # Scrolled without flame bonus
        return 0

    flameBonus = int(stats[2])

    print(stat + " +" + str(flameBonus))
    return flameBonus

def isStatVisible(stat, statsDict):
    return [value for key, value in statsDict.items() if stat.lower() in key.lower()]

def getFlameStats(lines):
    statsDict = {}
    for line in lines:
        matches = re.search(LINE_REGEX, line)
        if matches:
            statsDict[matches["stat"].strip()] = [matches["total"], matches["base"], matches["bonus1"], matches["bonus2"]]
    if MODE_CALIBRATE:
        print(statsDict)
    return statsDict

def getFlameText():
    img = np.array(pyautogui.screenshot(region=(FLAME_REGION_X,FLAME_REGION_Y, FLAME_WIDTH, FLAME_HEIGHT)))

    img = cv2.bitwise_not(img)
    size = 3
    img = cv2.resize(img, (FLAME_WIDTH * size, FLAME_HEIGHT * size), cv2.INTER_CUBIC)
    
    ret, img = cv2.threshold(img, 160,255, cv2.THRESH_BINARY)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, img = cv2.threshold(img, 160,255, cv2.THRESH_BINARY)
    
    # kernel = np.ones((2,2), np.uint8)
    # img = cv2.dilate(img, kernel, iterations = 1)

    # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    # img = cv2.erode(img, kernel)
    
    # cv2.imwrite(time.strftime("%Y%m%d-%H%M%S" + ".png"),img)

    text = pytesseract.image_to_string(img, config="--psm 6")
    text = os.linesep.join([s for s in text.splitlines() if s and s != " " and s != "  "])

    # Exclude lines after Potential
    lines_raw = text.split('\n')
    poten_index = index_containing_substring(lines_raw, "Poten")
    lines = lines_raw[:poten_index]

    # # Exclude lines after Remaining Enhancements
    remaining_index = index_containing_substring(lines, "Enhancements")
    if (remaining_index >= 0):
        lines = lines[:remaining_index]

    # Exclude lines before Type
    type_index = index_containing_substring(lines, "Type")
    lines = lines[type_index:]

    return img, lines

def countdownStart():
    print("Starting", end="")
    for i in range(0, 2):
        print(".", end="")
        time.sleep(1)
    print("Go")

def clearConsole():
    os.system('cls' if os.name == 'nt' else 'clear')

# https://stackoverflow.com/questions/2170900/get-first-list-index-containing-sub-string
def index_containing_substring(the_list, substring):
    for i, s in enumerate(the_list):
        if substring in s:
              return i
    return -1

if __name__ == "__main__":
    main()