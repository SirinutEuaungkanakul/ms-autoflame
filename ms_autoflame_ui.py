import pyautogui
import time
from random import randint

import numpy as np
import os
from mss import mss
import cv2
from PIL import Image
import pytesseract
import re
from termcolor import colored

# Setup default settings
# 0. See Tutorial1.png
# 1. Use 1366x768 windowed
# 2. Move game screen to top left of your screen
# 3. Move inventory screen to snap lock top left of screen
# 4. Put item in top left slot of EQUIP
# 5. Have red flame somewhere visible in USE
# 6. (Optional) Change Maplestory font to something more readable for better accuracy
# ---- https://forum.ragezone.com/f428/changing-maplestorys-font-695367/
# ---- Personally I use Tahoma Bold but it still isn't 100% accurate

# Usages
# 1. Adjust target and/or flamescore calculation
# ---1--- TARGET_FLAME_SCORE
# ---2--- def getTotalFlameScore(lines): - Customize flamescore calculation + specify if item is scrolled
# ---------- See Tutorial2.png
# 2. Make sure game screen is fully visible then run code with MODE_CALIBRATE = True,
# --- make sure the console display detected stats correctly
# 3. Change MODE_CALIBRATE = False then run the program again to start flaming

##### Settings #####
## For all possible combinations / Terminology see https://docs.google.com/document/d/1shvjaRT8I9McNmMHWHjILNdEidAZaw5wkkOrwm5gWW4/edit#
# Reference Target Flame Score (Equivalent flames are automatically captured)
# Item Max main stats 3L/4L
# lv120+HT 77/105
# lv140+Gollux/CRA 84/112
# lv160+Abso/Book/Berserk/EPatch 98/133
# lv200+Arcane/Dreamy 119/161
# Example score For arcane weapons:
# - Target T7/92 ATT => 92 * 3 = 276
# - Target T6 BOSS => 12% * 8 = 96
# - Target T6 DMG => 6% * 16 = 96
# - Target T5 AS => 5% * 9 = 45
# TARGET_FLAME_SCORE = 276 + 96 + 96 # Weapon T7 + T6 Boss + T6 DMG
TARGET_FLAME_SCORE = 120 + 63  # 128/7
AMOUNT_TO_FLAME = 20000
MODE_CALIBRATE = False  # True = View Only, False = AutoFlame
##### Target Stats #####

def getTotalFlameScore(statsDict):
    # getFlameStat(statsDict, <String to match>, <line is scrolled>) * <score per unit>
    STR = getFlameStat(statsDict, "STR") * 1
    DEX = getFlameStat(statsDict, "DEX") * 1
    INT = getFlameStat(statsDict, "INT") * 1
    LUK = getFlameStat(statsDict, "LUK") * 1
    ATT = getFlameStat(statsDict, "ack Power") * 3
    MATT = getFlameStat(statsDict, "Magic A") * 3
    ALL = getFlameStat(statsDict, "Stats") * 9

    # 6x to replace T7 Flat, 8x to prioritize
    BOSS = getFlameStat(statsDict, "Boss Damage") * 8
    # 12x to replace T7 Flat, 16x to prioritize
    DAMAGE = getFlameStat(statsDict, "Damage", exactMatch=True) * 16

    # Adjust your flame score calculation here (or Select one)

    # flameScore = STR + (0.1 * DEX) + ATT + ALL # STR Armor
    # flameScore = DEX + (0.1 * STR) + ATT + ALL # DEX Armor
    # flameScore = INT + (0.1 * LUK) + MATT + ALL # INT Armor
    # flameScore = LUK + (0.1 * DEX) + ATT + ALL # LUK Armor
    # flameScore = LUK + (0.1 * DEX) + (0.1 * STR) + ATT + ALL # Dagger Class Armor

    # flameScore = STR + (0.1 * DEX) + ATT + ALL + BOSS + DAMAGE # STR Weapon
    # flameScore = DEX + (0.1 * STR) + ATT + ALL + BOSS + DAMAGE # DEX Weapon
    # flameScore = INT + (0.1 * LUK) + MATT + ALL + BOSS + DAMAGE # INT Weapon
    # flameScore = LUK + (0.1 * DEX) + ATT + ALL + BOSS + DAMAGE # LUK Weapon
    # flameScore = LUK + (0.1 * DEX) + (0.1 * STR) + ATT + ALL + BOSS + DAMAGE # Dagger

    flameScore = max(  # Flame for any job
        STR + (0.1 * DEX) + ATT + ALL - 20,
        DEX + (0.1 * STR) + ATT + ALL - 20,
        INT + (0.1 * LUK) + MATT + ALL - 20,
        LUK + (0.1 * DEX) + (0.1 * STR) + ATT + ALL
    )

    return flameScore


##### Calibration/Performance #####
FAILSAFE = True  # End program by moving mouse to screen corner
STUCK_LIMIT = 40
DELAY_BEFORE_READING_FLAME = 0.9  # Delay to wait for server to update stats
KEEP_STATS_VISIBLE_DELAY = 0.3  # Extend the time the stats is manually visible

# Stats display on mouse hover
FLAME_REGION_X = 317
FLAME_REGION_Y = 344
FLAME_WIDTH = 168
FLAME_HEIGHT = 90

# EQUIP inventory tab
EQUIP_X = 26
EQUIP_Y = 61

# USE inventory tab
USE_X = 56
USE_Y = 61

# Slot to use flame on
SLOT_X = 34
SLOT_Y = 97

############### END Settings ###############

LINE_REGEX = r"(?P<stat>[^:+\n]*)[ :+#-]+(?P<bonus>\d+)[% ]*"

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


def main():
    pyautogui.FAILSAFE = FAILSAFE

    countdownStart()

    i = 0
    stuckCounter = STUCK_LIMIT
    flame = None
    lastScore = -1
    while True:
        print(colored("Checking flame: " + str(i), 'cyan'))

        if MODE_CALIBRATE:
            while True:
                time.sleep(1)
                img, lines = getFlameText()
                statsDict = getFlameStats(lines)
                cv2.imshow('test', img)
                if cv2.waitKey(1) != -1:
                    break
                clearConsole()

                print("Raw Text:")
                print(lines)

                print("Visible Stats:")
                print("Visible Flame Score: " + str(getTotalFlameScore(statsDict)))
            break

        # Check if GUI visible
        retry = pyautogui.locateCenterOnScreen(
                    'TRY_AGAIN.png', grayscale=True, region=(300, 300, 500, 500))
        if not retry:
            pyautogui.click(USE_X, USE_Y)
            if not flame:
                flame = pyautogui.locateCenterOnScreen(
                    'Flame.png', grayscale=True, region=(0, 0, 700, 500))
                print(flame)
                if not flame:
                    stuckCounter -= 1
                    if stuckCounter <= 0:
                        print("Force exit, No flames detected")
                        return
                    continue
            pyautogui.click(flame.x, flame.y, clicks=2)

            # Apply to item
            while not pyautogui.locateCenterOnScreen('TAB_EQUIP_SELECTED.png', grayscale=True, region=(0, 0, 100, 100)):
                stuckCounter -= 1
                if stuckCounter <= 0:
                    print("Force exit, could not use flame")
                    return

                print("Unstucking use flame")
                if stuckCounter % 5 == 2:
                    pyautogui.press('Enter')
                    pyautogui.typewrite('@check ' + str(randint(1, 9)))
                    pyautogui.press('Enter', presses=2)
                time.sleep(0.5)
                pyautogui.click(USE_X, USE_Y)
                flame = pyautogui.locateCenterOnScreen(
                    'Flame.png', grayscale=True, region=(0, 0, 700, 500))
                if not flame:
                    print("Force exit, No flames detected")
                    return
                pyautogui.click(flame.x, flame.y, clicks=2)
            pyautogui.click(SLOT_X, SLOT_Y)
            pyautogui.press('Enter', presses=2)
            time.sleep(DELAY_BEFORE_READING_FLAME)
            continue

        # Get Stats
        img, lines = getFlameText()
        statsDict = getFlameStats(lines)

        if not isStatsValid(lines, statsDict):
            stuckCounter -= 1
            if stuckCounter <= 0:
                print("Force exit, % stats failed")
                break
            print(colored("Invalid % stats. Rechecking...", 'red'))
            print(lines)
            time.sleep(0.2)
            continue
        stuckCounter = STUCK_LIMIT

        # Check stats
        flameScore = getTotalFlameScore(statsDict)

        print("Flame Score: " + str(flameScore) +
              " / " + str(TARGET_FLAME_SCORE))
        if flameScore == lastScore:
            # Stats not updated or exactly same score, recheck
            print(colored("Same score as last flame? Rechecking..", 'red'))
            lastScore = -1
            continue
        lastScore = flameScore
        if flameScore >= TARGET_FLAME_SCORE:
            break

        time.sleep(KEEP_STATS_VISIBLE_DELAY)

        # Reflame
        if i >= AMOUNT_TO_FLAME:
            print("out of flame budget :(")
            break

        pyautogui.click(retry.x, retry.y)
        pyautogui.press('Enter', presses=2)
        
        pyautogui.moveTo(20,60) # Move mouse somewhere else to not block image detection

        stuckCounter = STUCK_LIMIT
        i += 1
        time.sleep(DELAY_BEFORE_READING_FLAME)

    print("Done: ~" + str(i) + " Flames used")


def isStatsValid(lines, statsDict):
    if not isStatBetween(statsDict, "Stats", 0, 7):
        return False
    if not isStatBetween(statsDict, "Damage", 0, 7, exactMatch=True):
        return False
    if not isStatBetween(statsDict, "Boss Damage", 0, 14):
        return False

    return True


def isStatBetween(statsDict, stat, lower, higher, exactMatch=False):
    value = getFlameStat(statsDict, stat, exactMatch=exactMatch)
    return value >= lower and value <= higher
    

def isLineVisible(substring, lines):
    return any(substring.lower() in line.lower() for line in lines)


def getFlameStat(statsDict, stat, exactMatch=False):
    if not exactMatch:
        # Get first matching key containing substring stat
        matched_lines = getMatchedLines(stat, statsDict)
        if not matched_lines:
            return 0
        bonus = matched_lines[0]
    else:
        dictValues = statsDict.get(stat)
        if not dictValues:
            return 0
        bonus = dictValues

    flameBonus = int(bonus)

    print(stat + " +" + str(flameBonus))
    return flameBonus


def getMatchedLines(stat, statsDict):
    return [value for key, value in statsDict.items() if stat.lower() in key.lower()]


def getFlameStats(lines):
    statsDict = {}
    for line in lines:
        matches = re.search(LINE_REGEX, line)
        if matches:
            statsDict[matches["stat"].strip()] = matches["bonus"]
    if MODE_CALIBRATE:
        print(statsDict)
    return statsDict


def getFlameText():
    img = np.array(pyautogui.screenshot(
        region=(FLAME_REGION_X, FLAME_REGION_Y, FLAME_WIDTH, FLAME_HEIGHT)))

    img = cv2.bitwise_not(img)
    size = 3
    img = cv2.resize(
        img, (FLAME_WIDTH * size, FLAME_HEIGHT * size), cv2.INTER_CUBIC)

    ret, img = cv2.threshold(img, 160, 255, cv2.THRESH_BINARY)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, img = cv2.threshold(img, 160, 255, cv2.THRESH_BINARY)

    # kernel = np.ones((2,2), np.uint8)
    # img = cv2.dilate(img, kernel, iterations = 1)

    # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    # img = cv2.erode(img, kernel)

    # cv2.imwrite(time.strftime("%Y%m%d-%H%M%S" + ".png"),img)

    text = pytesseract.image_to_string(img, config="--psm 6")
    text = os.linesep.join(
        [s for s in text.splitlines() if s and s != " " and s != "  "])

    lines = text.split('\n')

    return img, lines


def countdownStart():
    print("Starting", end="")
    for i in range(0, 2):
        print(".", end="")
        time.sleep(1)
    print("Go")


def clearConsole():
    os.system('cls' if os.name == 'nt' else 'clear')


if __name__ == "__main__":
    main()
